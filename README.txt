Docu
============


Este paquete se hizo con zopeskell.diazotheme..

desde tu virtualenv (nuevo) se deben ejecutar una serie de pasos para instalalar las herramientas de desarrollo que generan
paquetes

1. pip install zopeskel==2.21.2 
2. pip install zopeskel.diazotheme


luego para generar el paquete desde la carpeta /proyecto/src

##ejecutar comandos

1. zopeskel diazotheme nombrepaquete (plonetheme.cursove)
esto genera una serie de archivos en la carpeta /proyecto/src/plonetheme.cursove

2. editar buildout e instalar en las variables (eggs y develop) el paquete recien generado 

3. ejecutar bin/buildout -vN

4. levantar tu instancia bin/instance fg

5. instalar el paquete en el sistema plone.. (configuracion del sitio/complementos) luego activar el tema en theming

6. bajarse un tema de cualquier direccion de temas ejemplo (http://www.freecsstemplates.org/)

7. descomprimir el tema y copiar el contenido en /proyecto/src/plonetheme.cursove/plonetheme/cursove/diazo_resources/
eso reeemplaza el archivo index.html (eso es normal)

8. refrescar tu sitio..

9. crear reglas diazo y adaptar el tema al sistema plone.
